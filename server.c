#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <dirent.h>
#include <arpa/inet.h>
#include <errno.h>
#include <ctype.h>
#include <sys/stat.h>
#include <fcntl.h>

#define SHMKEY ((key_t) 3399)
#define SEMKEY1 ((key_t) 7891)
#define SEMKEY2 ((key_t) 7892)
#define PERMS 0666
#define BIGCOUNT 31
#define CLIENT_CNT 30
int shm_id,clisem,servsem;

char* WELCOME_MSG = "****************************************\n** Welcome to the information server. **\n****************************************\n";
char exec_list[80][255] = {};
char cmd[5000][1024];
char env_name[100][500];
char env_argv[100][5000];
char orig_cmd[15000];
int env_argc = 0;
int cmd_cnt = 0;
int exec_list_cnt = 0;
int number_pipe_fd[3000];
int number_pipe_line[3000];
int number_pipe_cnt = 0;
int number_pipe_type_array[3000];

int client_cnt = 0;

static struct sembuf op_lock[2] = 
{
	2, 0, 0,
	2, 1, SEM_UNDO
};

static struct sembuf op_endcreate[2] = 
{
	1, -1, SEM_UNDO,
	2, -1, SEM_UNDO
};

static struct sembuf op_open[1] = { 1, -1, SEM_UNDO};

static struct sembuf op_close[3] = 
{
	2, 0, 0, 
	2, 1, SEM_UNDO, 
	1, 1, SEM_UNDO 
};

static struct sembuf op_unlock[1] = {2, -1, SEM_UNDO};

static struct sembuf op_op[1] = {0, 30, SEM_UNDO};

struct clientInfo
{
	int pid;
	int id;
	int msg_cnt;
	int unread_cnt;
	int client_port;
	char client_ip[50];
	char client_name[20];
	char chat_buf[10][1024];
	char unread_buf[10][1024];
	
};

typedef struct 
{
	struct clientInfo clients[CLIENT_CNT];
	int client_cnt;
	int fifo_cnt;
	int fifo_src[100];
	int fifo_dest[100];

} shm_msg;
shm_msg *shm_ptr;


union semun 
{
	int val;
	struct semid_ds *buff;
	ushort *array;
};

void error(const char *msg)
{
	perror(msg);
	exit(1);
}

void parse_cmd(char* cmd_input)
{
	
	cmd_cnt = 0;

	char cmd_line[15000];

	strcpy(cmd_line,cmd_input);
	for(int i=0;i<15000;++i)
	{
		if(cmd_line[i] == '\n' || cmd_line[i] == '\r')
			cmd_line[i] = ' ';
	}

	char* token = strtok(cmd_line," ");
	for(int i=0;i<2000;i++)
		bzero(cmd[i],1024);

	if(strcmp(token,"yell") == 0)
	{
		strcpy(cmd[0],token);
		int pos = 0;
		int msg_it = 0;
		char c;
		char msg[1024];
		bzero(msg,1024);
		for(int i=0;i<strlen(cmd_input);++i)
		{
			if(cmd_input[i] == 'y')
			{
				pos = i;
				break;
			}
		}

		for(int i=pos+4;i<strlen(cmd_input);++i)
		{
			if(isalpha(cmd_input[i]) != 0)
			{
				pos = i;
				break;
			}
		}

		for(int i=pos;i<strlen(cmd_input);++i)
		{
			if(cmd_input[i] != '\r' && cmd_input[i] != '\n')
			{
				msg[msg_it] = cmd_input[i];
				msg_it++;
			}	
		}

		strcpy(cmd[1],msg);
		cmd_cnt = 2;
		return;
	}
	else if(strcmp(token,"tell") == 0)
	{
		strcpy(cmd[0],token);
		token = strtok(NULL," ");
		strcpy(cmd[1],token);

		int pos = 0;
		int msg_it = 0;
		char c;
		char msg[1024];
		bzero(msg,1024);
		for(int i=0;i<strlen(cmd_input);++i)
		{
			if(cmd_input[i] == 't')
			{
				pos = i;
				break;
			}
		}

		for(int i=pos+4;i<strlen(cmd_input);++i)
		{
			if(isalpha(cmd_input[i]) != 0)
			{
				pos = i;
				break;
			}
		}

		for(int i=pos;i<strlen(cmd_input);++i)
		{
			if(cmd_input[i] != '\r' && cmd_input[i] != '\n')
			{
				msg[msg_it] = cmd_input[i];
				msg_it++;
			}	
		}

		strcpy(cmd[2],msg);
		cmd_cnt = 3;
		return;
	}

	while(token != NULL)
	{

		if(strcmp(token,"\n") == 0 || strcmp(token,"\r") == 0)
		{
			token = strtok(NULL," ");
			continue;
		}

		if(strncmp(token,"|",1) == 0 && strlen(token) > 1)
		{
			strcpy(cmd[cmd_cnt],"|");

			char temp[256] = "";
			for(int i=0;i<strlen(token)-1;i++)
				temp[i] = token[i+1];
			
			strcpy(cmd[cmd_cnt+1],temp);
			cmd_cnt = cmd_cnt + 2;
		}
		else if(strncmp(token,"!",1) == 0 && strlen(token) > 1)
		{

			strcpy(cmd[cmd_cnt],"!");

			char temp[256] = "";
			for(int i=0;i<strlen(token)-1;i++)
				temp[i] = token[i+1];
			
			strcpy(cmd[cmd_cnt+1],temp);
			cmd_cnt = cmd_cnt + 2;
		}
		else if(strncmp(token,">",1) == 0 && strlen(token) > 1)
		{
			strcpy(cmd[cmd_cnt],">");

			char temp[256] = "";
			for(int i=0;i<strlen(token)-1;i++)
				temp[i] = token[i+1];
			
			strcpy(cmd[cmd_cnt+1],temp);
			cmd_cnt = cmd_cnt + 2;
		}
		else if(strncmp(token,"<",1) == 0 && strlen(token) > 1)
		{
			strcpy(cmd[cmd_cnt],"<");

			char temp[256] = "";
			for(int i=0;i<strlen(token)-1;i++)
				temp[i] = token[i+1];
			
			strcpy(cmd[cmd_cnt+1],temp);
			cmd_cnt = cmd_cnt + 2;
		}
		else
		{
			strcpy(cmd[cmd_cnt],token);
			cmd_cnt = cmd_cnt + 1;
		}
		
		token = strtok(NULL," ");
	}

}


void scan_bin()
{
	DIR *dir;
	struct dirent *ent;
	char scan_path[10000] = "/net/gcs/105/0556506/ras/";
	char temp[1000];
	strcpy(temp,env_argv[0]);
	char* path_token = strtok(temp,":");
	exec_list_cnt = 0;

	while(path_token != NULL)
	{
		strcpy(scan_path,path_token);
		if ((dir = opendir (scan_path)) != NULL) 
		{
			/* print all the files and directories within directory */
			while ((ent = readdir (dir)) != NULL) 
			{
				if(strncmp(ent->d_name,".",1) != 0 && strncmp(ent->d_name,"..",2) != 0)
				{
					strcpy(exec_list[exec_list_cnt],ent->d_name);
					++exec_list_cnt;
				}
			}
			closedir(dir);
		} 
		else
		{
			/* could not open directory */
			perror ("");
		}
		path_token = strtok(NULL,":");
	}
	
}

int check_exist(char* check_cmd)
{
	scan_bin();
	for(int i=0;i<exec_list_cnt;i++)
	{
		if(strcmp(check_cmd,exec_list[i]) == 0)
			return 1;
	}
	return 0;
}

/* These sem functions are from the course slides */
int sem_create(key_t key, int initval)
{
	register int id, semval;
	union semun semctl_arg;
	if(key == IPC_PRIVATE) return -1; /* not intended  for private sem */
	else if(key == (key_t) -1 ) return -1; /* provaly an ftok() error by caller */
	
	again:
		if( (id = semget(key, 3, PERMS | IPC_CREAT)) <0 ) return -1;
		if( (semop(id, &op_lock[0], 2)) <0 )
		{
			if(errno == EINVAL) goto again;
			perror("can't lock");
		}
		if((semval = semctl(id, 1, GETVAL, 0)) <0 ) perror("can't GETVAL");
		if(semval == 0) /* initial state */
		{
			semctl_arg.val = initval;
			if(semctl(id, 0, SETVAL, semctl_arg) <0 ) perror("can't SETVAL[0] ");
			semctl_arg.val = BIGCOUNT;/* at most are 30 client to attach this file */
			if(semctl(id, 1, SETVAL, semctl_arg) <0 ) perror("can't SETVAL[1] ");
		}
		
		if(semop(id, &op_endcreate[0], 2) <0 )
			perror("can't end create ");
		
		return id; }

int sem_rm(int id)
{
	if(semctl(id, 0, IPC_RMID, 0) <0)
		perror("can't IPC_RMID");
}

int sem_open(key_t key)
{
	register int id;
	if(key == IPC_PRIVATE) return -1;
	else if(key == (key_t) -1 ) return -1;
	
	if((id = semget(key, 3, 0)) <0 ) return -1; /* doesn't exist or tables full*/
	
	if(semop(id, &op_open[0], 1) <0 ) perror("can't open ");
	
	return id;
}

void sem_close(int id)
{
	register int semval;
	if(semop(id, &op_close[0], 3) <0 ) perror("can't semop ");
	
	if((semval = semctl(id, 1, GETVAL, 0)) <0) perror("can't GETVAL");
	if(semval > BIGCOUNT) perror("sem[1] > BIGCOUNT ");
	else if(semval == BIGCOUNT) sem_rm(id);
	else
		if(semop(id, &op_unlock[0],1) <0) perror("can't unlock ");
}

void sem_op(int id, int value)
{
	if((op_op[0].sem_op = value) == 0) perror("can't have value == 0 ");
	if(semop(id, &op_op[0], 1) <0 ) perror("sem_op error ");
}

void sem_wait(int id)
{
	sem_op(id, -1);
}

void sem_signal(int id)
{
	sem_op(id, 1);
}

void zombie(int s)
{
	while (waitpid(-1, NULL, WNOHANG) > 0);
}

void write_buf(int s)
{
	char buf[20];

	sem_wait(clisem);
	
	for(int i = 0; i<shm_ptr->client_cnt; i++)
	{

		if(shm_ptr->clients[i].id != -1 && (getpid() == shm_ptr->clients[i].pid) )
		{
			for(int k=0; k<shm_ptr->clients[i].msg_cnt; k++)
			{
				write(4,shm_ptr->clients[i].chat_buf[k],strlen(shm_ptr->clients[i].chat_buf[k]));
				strcpy(shm_ptr->clients[i].chat_buf[k],"");
			}
			shm_ptr->clients[i].msg_cnt = 0;
			break;
		}
	}
	
	sem_signal(clisem);
}

void write_unread_buf(int s)
{
	char buf[20];

	sem_wait(clisem);
	
	for(int i = 0; i<shm_ptr->client_cnt; i++)
	{

		if(shm_ptr->clients[i].id != -1 && (getpid() == shm_ptr->clients[i].pid) )
		{
			for(int k=0; k<shm_ptr->clients[i].unread_cnt; k++)
			{
				write(4,shm_ptr->clients[i].unread_buf[k],strlen(shm_ptr->clients[i].unread_buf[k]));
				strcpy(shm_ptr->clients[i].unread_buf[k],"");
			}
			shm_ptr->clients[i].unread_cnt = 0;
			break;
		}
	}
	
	sem_signal(clisem);
}

void clear_ipc_files(int id)
{
	char ipc_path[1000];
	
	sem_wait(clisem);

	for(int i=0;i<shm_ptr->fifo_cnt;++i)
	{
		if(shm_ptr->fifo_src[i] == id || shm_ptr->fifo_dest[i] == id)
		{

			sprintf(ipc_path,"/net/gcs/105/0556506/tmp/w%dto%d",shm_ptr->fifo_src[i],shm_ptr->fifo_dest[i]);
			remove(ipc_path);
			shm_ptr->fifo_src[i] = -1;
			shm_ptr->fifo_dest[i] = -1;
		}
		
	}

	int temp = shm_ptr->fifo_cnt-1;
	for(int i=0;i<temp;++i)
	{
		if(shm_ptr->fifo_src[i] == -1 || shm_ptr->fifo_dest[i] == -1)
		{
			shm_ptr->fifo_src[i] = shm_ptr->fifo_src[i+1] ;
			shm_ptr->fifo_dest[i] = shm_ptr->fifo_dest[i+1];
			shm_ptr->fifo_cnt = shm_ptr->fifo_cnt - 1;

		}	
	}

	sem_signal(clisem);

}
int main(int argc, char *argv[])
{
	chdir("/net/gcs/105/0556506/ras");

	signal(SIGCHLD, zombie);
	signal(SIGUSR1, write_buf);
	signal(SIGUSR2, write_unread_buf);

	int sockfd, newsockfd, portno;
	socklen_t clilen;
	char buffer[15000];
	struct sockaddr_in serv_addr, cli_addr;
	int n;

	if (argc < 2) {
	 fprintf(stderr,"ERROR, no port provided\n");
	 exit(1);
	}

	strcpy(env_name[0],"PATH");
	strcpy(env_argv[0],"bin:."); 
	env_argc = 1;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);

	int enable = 1;
	if (sockfd < 0 || setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&enable,sizeof(int))<0) 
		error("ERROR opening socket");

	bzero((char *) &serv_addr, sizeof(serv_addr));
	portno = atoi(argv[1]);

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);

	if (bind(sockfd, (struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
		error("ERROR on binding");

	if((shm_id = shmget(SHMKEY, sizeof(shm_msg), PERMS|IPC_CREAT))<0)
		perror("shmget error ");


	if( (shm_ptr = (shm_msg*) shmat(shm_id, (char*)0, 0)) == (shm_msg *) -1)
		perror("shmat error");
	
	if( (clisem = sem_create(SEMKEY1,1))<0) perror("client sem");
	
	shm_ptr->client_cnt = 0;
	shm_ptr->fifo_cnt = 0;

	while(1)
	{
		
		int current_id = -1;
		listen(sockfd,5);
		clilen = sizeof(cli_addr);
		newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
	
		if (newsockfd < 0) 
		  error("ERROR on accept");
		
		n = write(newsockfd,WELCOME_MSG,strlen(WELCOME_MSG));
		if (n < 0) error("ERROR writing to socket");

		
		switch(fork())
		{
			case 0:
				close(sockfd);

				sem_wait(clisem);

				int struct_ptr = shm_ptr->client_cnt;

				for(int i=0;i<shm_ptr->client_cnt;++i)
				{
					if(shm_ptr->clients[i].id == -1)
					{
						struct_ptr = i;
						break;
					}
				}

				shm_ptr->clients[struct_ptr].pid = getpid();
				shm_ptr->clients[struct_ptr].msg_cnt = 0;
				shm_ptr->clients[struct_ptr].unread_cnt = 0;
				shm_ptr->clients[struct_ptr].id = struct_ptr;
				
				strcpy(shm_ptr->clients[struct_ptr].client_ip,inet_ntoa(cli_addr.sin_addr));
				shm_ptr->clients[struct_ptr].client_port = (int) ntohs(cli_addr.sin_port);
				strcpy(shm_ptr->clients[struct_ptr].client_name,"(no name)");
				
				current_id = struct_ptr;

				

				bzero(buffer,15000);
				sprintf(buffer,"*** User '%s' entered from %s/%d. ***\n",shm_ptr->clients[current_id].client_name,shm_ptr->clients[current_id].client_ip,shm_ptr->clients[current_id].client_port);
				
				if( struct_ptr == shm_ptr->client_cnt)
					shm_ptr->client_cnt = shm_ptr->client_cnt + 1;
				
				for(int i=0;i<shm_ptr->client_cnt;++i)
				{		
					strcpy(shm_ptr->clients[i].chat_buf[shm_ptr->clients[i].msg_cnt],buffer);
					shm_ptr->clients[i].msg_cnt = shm_ptr->clients[i].msg_cnt+ 1;
				}
 				
				sem_signal(clisem);
				
				kill(0,SIGUSR1);
				

				while(1)
				{
					n = write(newsockfd,"% ",2);
					if (n < 0) error("ERROR writing to socket");

					bzero(buffer,15000);
					n = read(newsockfd,buffer,15000);
					
					if (n < 0) error("ERROR reading from socket");

					strcpy(orig_cmd,buffer);
					for(int i=0;i<strlen(orig_cmd);++i)
					{
						if( orig_cmd[i] == '\r' || orig_cmd[i] == '\n' )
							orig_cmd[i] = 0;
					}

					parse_cmd(buffer);

					if(strncmp(cmd[0],"exit",4) == 0)
					{
						number_pipe_cnt = 0;
						sem_wait(clisem);
						
						bzero(buffer,15000);
						sprintf(buffer,"*** User '%s' left. ***\n",shm_ptr->clients[current_id].client_name);
						
						
						for(int i=0;i<shm_ptr->client_cnt;++i)
						{
							strcpy(shm_ptr->clients[i].chat_buf[shm_ptr->clients[i].msg_cnt],buffer);
							shm_ptr->clients[i].msg_cnt = shm_ptr->clients[i].msg_cnt + 1;
						}

						sem_signal(clisem);

						kill(0,SIGUSR1);

						close(newsockfd);

						clear_ipc_files(shm_ptr->clients[current_id].id+1);

						sem_wait(clisem);
						shm_ptr->clients[current_id].id = -1;

						sem_signal(clisem);

						
						break;
					}
					else if(strncmp(cmd[0],"setenv",6) == 0)
					{
						int env_exist = 0;
						for(int i=0;i<env_argc;++i)
						{
							if(strcmp(env_name[i],cmd[1]) == 0)
							{
								strcpy(env_argv[i],cmd[2]);
								env_exist = 1;
								break;
							}
						}

						if(env_exist == 0)
						{
							strcpy(env_name[env_argc],cmd[1]);
							strcpy(env_argv[env_argc],cmd[2]);
							env_argc++;
						}
						
						continue;		
					}
					else if(strncmp(cmd[0],"printenv",8) == 0)
					{				
						
						for(int i=0;i<env_argc;++i)
						{
							if(strcmp(env_name[i],cmd[1]) == 0)
							{
								char temp[10000];
								strcpy(temp,"");
								strcat(temp,env_name[i]);
								strcat(temp,"=");
								strcat(temp,env_argv[i]);
								strcat(temp,"\n");

								n = write(newsockfd,temp,strlen(temp));
								if (n < 0) error("ERROR writing to socket");

								break;
							}
						}

						continue;
					}
					else if(strncmp(cmd[0],"tell",4) == 0)
					{
						int client_exist = -1;
						bzero(buffer,15000);

						sem_wait(clisem);
						for(int i=0;i<shm_ptr->client_cnt;++i)
						{
							if(shm_ptr->clients[i].id == -1)
								continue;
							if(shm_ptr->clients[i].id == (atoi(cmd[1])-1))
							{
								client_exist = i;
								break;
							}
						}

						if(client_exist == -1)
						{

							sprintf(buffer,"*** Error: user #%s does not exist yet. ***\n",cmd[1]);

							if(shm_ptr->clients[current_id].msg_cnt < 10)
							{
								strcpy(shm_ptr->clients[current_id].chat_buf[shm_ptr->clients[current_id].msg_cnt],buffer);
								shm_ptr->clients[current_id].msg_cnt = shm_ptr->clients[current_id].msg_cnt + 1;
								sem_signal(clisem);

								kill(shm_ptr->clients[current_id].pid,SIGUSR1);
							}
							else sem_signal(clisem);
							

							

						}
						else
						{
							sprintf(buffer,"*** %s told you ***: %s\n",shm_ptr->clients[current_id].client_name,cmd[2]);

							if(shm_ptr->clients[client_exist].unread_cnt < 10)
							{
								strcpy(shm_ptr->clients[client_exist].unread_buf[shm_ptr->clients[client_exist].unread_cnt],buffer);
								shm_ptr->clients[client_exist].unread_cnt = shm_ptr->clients[client_exist].unread_cnt + 1;

								sem_signal(clisem);

								kill(shm_ptr->clients[client_exist].pid,SIGUSR2);
							}
							else sem_signal(clisem);
							

						}

						continue;
						
					}
					else if(strncmp(cmd[0],"yell",4) == 0)
					{
						
						bzero(buffer,15000);

						char temp_msg[1024];
						strcpy(temp_msg,cmd[1]);

						for(int i=2;i<cmd_cnt;++i)
						{
							strcat(temp_msg," ");
							strcat(temp_msg,cmd[i]);
						}

						sem_wait(clisem);

						sprintf(buffer,"*** %s yelled ***: %s\n",shm_ptr->clients[current_id].client_name,cmd[1]);

						for(int i=0;i<shm_ptr->client_cnt;++i)
						{
							if(shm_ptr->clients[i].id == -1)
								continue;
							if(shm_ptr->clients[i].unread_cnt < 10)
							{
								strcpy(shm_ptr->clients[i].unread_buf[shm_ptr->clients[i].unread_cnt],buffer);
								shm_ptr->clients[i].unread_cnt = shm_ptr->clients[i].unread_cnt + 1;
							}
							
						}

						sem_signal(clisem);

						kill(0,SIGUSR2);

						continue;
						
					}
					else if(strncmp(cmd[0],"who",3) == 0)
					{
						
						bzero(buffer,15000);
						strcpy(buffer,"<ID>\t<nickname>\t<IP/port>\t<indicate me>\n");
						sem_wait(clisem);
						
						for(int i=0;i<shm_ptr->client_cnt;++i)
						{
							if(shm_ptr->clients[i].id == -1)
								continue;
							char temp_who[500];
							if(shm_ptr->clients[i].pid == getpid())
								sprintf(temp_who,"%d\t%s\t%s/%d\t%s\n",shm_ptr->clients[i].id+1,shm_ptr->clients[i].client_name,shm_ptr->clients[i].client_ip,shm_ptr->clients[i].client_port,"<-me");
							else
								sprintf(temp_who,"%d\t%s\t%s/%d\t%s\n",shm_ptr->clients[i].id+1,shm_ptr->clients[i].client_name,shm_ptr->clients[i].client_ip,shm_ptr->clients[i].client_port,"");
							strcat(buffer,temp_who);
						}

						if(shm_ptr->clients[current_id].msg_cnt < 10)
						{
							strcpy(shm_ptr->clients[current_id].chat_buf[shm_ptr->clients[current_id].msg_cnt],buffer);
							shm_ptr->clients[current_id].msg_cnt = shm_ptr->clients[current_id].msg_cnt + 1;

							sem_signal(clisem);

							kill(shm_ptr->clients[current_id].pid,SIGUSR1);
						}
						else sem_signal(clisem);

						continue;
					}
					else if(strncmp(cmd[0],"name",4) == 0)
					{
						int user_exist = 0;
						bzero(buffer,15000);
						sem_wait(clisem);
						for(int i=0;i<shm_ptr->client_cnt;++i)
						{
							if(shm_ptr->clients[i].id == -1)
								continue;

							if(strcmp(cmd[1],shm_ptr->clients[i].client_name) == 0)
							{
								user_exist = 1;
								sprintf(buffer,"*** User '%s' already exists. ***\n",cmd[1]);
								if(shm_ptr->clients[current_id].msg_cnt < 10)
								{
									strcpy(shm_ptr->clients[current_id].chat_buf[shm_ptr->clients[current_id].msg_cnt],buffer);
									shm_ptr->clients[current_id].msg_cnt = shm_ptr->clients[current_id].msg_cnt + 1;
									sem_signal(clisem);

									kill(shm_ptr->clients[current_id].pid,SIGUSR1);
								}
								else sem_signal(clisem);
								
								break;
							}
						}
						if(user_exist == 1)
							continue;						
						strcpy(shm_ptr->clients[current_id].client_name,cmd[1]);
						
						sprintf(buffer,"*** User from %s/%d is named '%s'. ***\n",shm_ptr->clients[current_id].client_ip,shm_ptr->clients[current_id].client_port,cmd[1]);

						for(int i=0;i<shm_ptr->client_cnt;++i)
						{
							if(shm_ptr->clients[i].id == -1)
								continue;
							if(shm_ptr->clients[i].msg_cnt < 10)
							{
								strcpy(shm_ptr->clients[i].chat_buf[shm_ptr->clients[i].msg_cnt],buffer);
								shm_ptr->clients[i].msg_cnt = shm_ptr->clients[i].msg_cnt + 1;
							}
							
						}

						sem_signal(clisem);

						kill(0,SIGUSR1);

						continue;
					}

					
					
					/* count how many pipe in this command */
					int pipe_number = 1;
					
					for(int i=0;i<cmd_cnt;++i)
					{
						if(strncmp(cmd[i],"|",1) == 0 || strncmp(cmd[i],"!",1) == 0)
							++pipe_number;
					}

					
					
					/* check if this command write to file or FIFO write */		
					int write_file = 0;
					int dest_exist = 0;
					int fifo_write = 0;
					int fifo_dest_id = 0;
					int fifo_wrtie_exist = 0;
					int write_file_pos = -1;
					char file_name[10000] ="";
					

					for(int i=0;i<cmd_cnt;++i)
					{
						if(strcmp(cmd[i],">") == 0)
						{
							write_file_pos = i;
							break;
						}
					}
					
					if(write_file_pos != -1)
					{
						
						for(int k=0;k<strlen(cmd[write_file_pos+1]);k++)
						{
							if(isdigit(cmd[write_file_pos+1][k]) == 0)
							{
								write_file = 1;
								break;
							}
						}
						
						if(write_file == 1)
						{
							strcpy(file_name,cmd[write_file_pos+1]);
							dest_exist = 1;	
						}
						else if(write_file == 0)
						{						
							fifo_write = 1;
							fifo_dest_id = atoi(cmd[write_file_pos+1]);

							for(int k=0;k<shm_ptr->client_cnt;++k)
							{
								if(shm_ptr->clients[k].id == (fifo_dest_id-1))
								{
									dest_exist = 1;
									break;
								}
							}

							if(dest_exist == 0)
							{
								bzero(buffer,15000);

								sem_wait(clisem);

								sprintf(buffer,"*** Error: user #%d does not exist yet. ***\n",fifo_dest_id);

								if(shm_ptr->clients[current_id].msg_cnt < 10)
								{
									strcpy(shm_ptr->clients[current_id].chat_buf[shm_ptr->clients[current_id].msg_cnt],buffer);
									shm_ptr->clients[current_id].msg_cnt = shm_ptr->clients[current_id].msg_cnt + 1;

									sem_signal(clisem);

									kill(shm_ptr->clients[current_id].pid,SIGUSR1);
								}
								else sem_signal(clisem);
								
							}

							for(int k=0;k<shm_ptr->fifo_cnt;k++)
							{
								if(shm_ptr->fifo_dest[k] == fifo_dest_id && shm_ptr->fifo_src[k] == current_id+1)
								{
									fifo_wrtie_exist = 1;
									bzero(buffer,15000);

									sem_wait(clisem);

									sprintf(buffer,"*** Error: the pipe #%d->#%d already exists. ***\n",shm_ptr->fifo_src[k],shm_ptr->fifo_dest[k]);

									if(shm_ptr->clients[current_id].msg_cnt < 10)
									{
										strcpy(shm_ptr->clients[current_id].chat_buf[shm_ptr->clients[current_id].msg_cnt],buffer);
										shm_ptr->clients[current_id].msg_cnt = shm_ptr->clients[current_id].msg_cnt + 1;

										sem_signal(clisem);

										kill(shm_ptr->clients[current_id].pid,SIGUSR1);
									}
									else sem_signal(clisem);
								}
							}

							if(fifo_wrtie_exist == 0 && dest_exist == 1)
							{
								shm_ptr->fifo_src[shm_ptr->fifo_cnt] = current_id+1;
								shm_ptr->fifo_dest[shm_ptr->fifo_cnt] = fifo_dest_id;
								shm_ptr->fifo_cnt = shm_ptr->fifo_cnt + 1;

								bzero(buffer,15000);

								sem_wait(clisem);

								sprintf(buffer,"*** %s (#%d) just piped '%s' to %s (#%d) ***\n",shm_ptr->clients[current_id].client_name,current_id+1,orig_cmd,shm_ptr->clients[fifo_dest_id-1].client_name,fifo_dest_id);
								
								for(int i=0;i<shm_ptr->client_cnt;++i)
								{
									if(shm_ptr->clients[i].id == -1)
										continue;
									if(shm_ptr->clients[i].msg_cnt < 10)
									{
										strcpy(shm_ptr->clients[i].chat_buf[shm_ptr->clients[i].msg_cnt],buffer);
										shm_ptr->clients[i].msg_cnt = shm_ptr->clients[i].msg_cnt + 1;
									}
									
								}

								sem_signal(clisem);

								kill(0,SIGUSR1);

							}
						}

						for(int i=write_file_pos;i<cmd_cnt-1;++i)
						{
							strcpy(cmd[i],cmd[i+1]);
						}
						cmd_cnt = cmd_cnt - 2;
					}
					else dest_exist = 1;
					
					if(fifo_wrtie_exist == 1 || dest_exist == 0)
						continue;

					/* check if this command is FIFO read or not */
					int fifo_read = 0;
					int fifo_src_id = 0;
					int fifo_read_exist = 0;
					int fifo_pipe[2];
					int read_pos = -1;

					for(int i=0;i<cmd_cnt;++i)
					{
						if(strncmp(cmd[i],"<",1) == 0)
						{
							read_pos = i;
							break;
						}
					}

					if(read_pos != -1)
					{

						fifo_read = 1;
						fifo_src_id = atoi(cmd[read_pos+1]);
						
						for(int i=0;i<shm_ptr->fifo_cnt;++i)
						{
							if(fifo_src_id == shm_ptr->fifo_src[i] && shm_ptr->fifo_dest[i] == current_id+1)
							{
								fifo_read_exist = 1;
								for(int k=i;k<(shm_ptr->fifo_cnt-1);++k)
								{
									shm_ptr->fifo_src[k] = shm_ptr->fifo_src[k+1];
									shm_ptr->fifo_dest[k] = shm_ptr->fifo_dest[k+1];
								}

								shm_ptr->fifo_cnt = shm_ptr->fifo_cnt - 1;
								break;
							}
						}
						
						if(fifo_read_exist == 0)
						{
							bzero(buffer,15000);

							sem_wait(clisem);

							sprintf(buffer,"*** Error: the pipe #%d->#%d does not exist yet. ***\n",fifo_src_id,current_id+1);

							if(shm_ptr->clients[current_id].msg_cnt < 10)
							{
								strcpy(shm_ptr->clients[current_id].chat_buf[shm_ptr->clients[current_id].msg_cnt],buffer);
								shm_ptr->clients[current_id].msg_cnt = shm_ptr->clients[current_id].msg_cnt + 1;

								sem_signal(clisem);

								kill(shm_ptr->clients[current_id].pid,SIGUSR1);
							}
							else sem_signal(clisem);
							

							continue;
						}
						else
						{
							char broadcast_buf[15000];
							bzero(broadcast_buf,15000);
							int find_index = 0;
							for(int i=0;i<shm_ptr->client_cnt;++i)
							{
								if( (fifo_src_id-1) == shm_ptr->clients[i].id )
								{
									find_index = i;
									break;
								}
							}

							sem_wait(clisem);
							sprintf(broadcast_buf,"*** %s (#%d) just received from %s (#%d) by '%s' ***\n",shm_ptr->clients[current_id].client_name,current_id+1,shm_ptr->clients[find_index].client_name,fifo_src_id,orig_cmd);
							for(int i=0;i<shm_ptr->client_cnt;++i)
							{

								if(shm_ptr->clients[i].id == -1)
									continue;
								if(shm_ptr->clients[i].msg_cnt < 10)
								{
									strcpy(shm_ptr->clients[i].chat_buf[shm_ptr->clients[i].msg_cnt],broadcast_buf);
									shm_ptr->clients[i].msg_cnt = shm_ptr->clients[i].msg_cnt + 1;
								}
							}
							sem_signal(clisem);
							kill(0,SIGUSR1);

							FILE* f;
							char fifo_read_path[100];

							sprintf(fifo_read_path,"/net/gcs/105/0556506/tmp/w%dto%d",fifo_src_id,current_id+1);
							

							f = fopen(fifo_read_path,"r");

							fseek(f, 0, SEEK_END);
							long fsize = ftell(f);
							fseek(f, 0, SEEK_SET);  

							char *str = malloc(fsize + 1);
							fread(str, fsize, 1, f);

							pipe(fifo_pipe);
							

							if(write(fifo_pipe[1],str,strlen(str)) < 0)
								perror("FIFO Pipe write error\n");
							
							fclose(f);
							remove(fifo_read_path);
						 	close(fifo_pipe[1]);
						}

						for(int i=read_pos;i<cmd_cnt-1;++i)
						{
							strcpy(cmd[i],cmd[i+1]);
						}
						
						cmd_cnt = cmd_cnt-2;

					}
				 
				 	/* check if there is any number pipe going to output */
				 	for(int i=0;i<number_pipe_cnt;++i)
				 	{
			  			if(number_pipe_line[i] > -1)
				 			number_pipe_line[i] =  number_pipe_line[i] - 1;
				 	}
					

				 	/* if there is some number pipe counts to 0, then pipe data into stdin */
					int number_pipe_out_cnt = 0;
					int np_out[2];
					pipe(np_out);
					
					if(number_pipe_cnt > 0)
					{
						for(int i=0;i<number_pipe_cnt;++i)
						{

							if(number_pipe_line[i] == 0)
							{
								bzero(buffer,15000);
								if(read(number_pipe_fd[i],buffer,sizeof(buffer)) < 0)
									printf("Number Pipe read error\n");

								if(write(np_out[1],buffer,strlen(buffer)) < 0)
									printf("Number Pipe write error\n");

								

								close(number_pipe_fd[i]);
								number_pipe_out_cnt++;
							}
						}
					}
					if(number_pipe_out_cnt == 0)
					{
						close(np_out[0]);
					}
				 	
				 	close(np_out[1]);
					

					/* check if this command cotains number pipe or not */
					int number_pipe = 1;
					int number_pipe_type = 0;

					
					if(strncmp(cmd[cmd_cnt-2],"|",1) == 0 || strncmp(cmd[cmd_cnt-2],"!",1) == 0)
					{

						for(int i=0;i<strlen(cmd[cmd_cnt-1]);++i)
						{
							if(isdigit(cmd[cmd_cnt-1][i]) == 0)
								number_pipe = 0;
						}
					}
					else number_pipe = 0;

					if(number_pipe == 1 && strncmp(cmd[cmd_cnt-2],"!",1) == 0)
					{
						number_pipe_type = 1;
					}
					

					int pfd[2];
					

					int fd_in = 0;
					int pre_fd_in = -1;
					int end_by_unknown = 0;
					int pipe_symbol = 0;
					int end_by_pipe = 0;
					
					
					pid_t pid;	

					
					for(int do_pipe_cnt = 0;do_pipe_cnt < pipe_number;do_pipe_cnt++)
					{
						
						char* exec_argv[cmd_cnt];
						int exec_argc = 0;
						int err_pfd[2];
						end_by_pipe = 0;
						
						for(int j=pipe_symbol;j<cmd_cnt;++j)
						{

							if(strncmp(cmd[j],"|",1) == 0 || strncmp(cmd[j],"!",1) == 0)
							{
								exec_argv[exec_argc] = NULL;
								++exec_argc;
								pipe_symbol = j+1;
								end_by_pipe = 1;
								break;
							}
							else
							{
								exec_argv[exec_argc] = cmd[j];
								++exec_argc;
								if(j == cmd_cnt-1)
								{
									exec_argv[exec_argc] = NULL;
									++exec_argc;
								}
							}
						}
						

						if(check_exist(exec_argv[0]) == 1)
						{

							char exec_path[5000] = "/net/gcs/105/0556506/ras/bin/";
							strcat(exec_path,exec_argv[0]);
							
							

							if (pipe(pfd)<0)
							{
								perror("pipe failed");
								exit(1);
							}

							if(pipe(err_pfd) < 0)
							{
								perror("pipe failed");
								exit(1);
							}
							
							pid = fork();

							if(pid<0) 
							{
								fprintf(stderr, "Fork Failed");
								exit(-1);

							}
							else if(pid==0) //child process
							{
								close(pfd[0]);
								close(err_pfd[0]);

								if(number_pipe_out_cnt > 0)
								{
									dup2(np_out[0],0);		
								}	
								else 
								{
									dup2(fd_in, 0);	
								}

								if(fifo_read_exist == 1)
								{
									dup2(fifo_pipe[0],0);
								}

								dup2(pfd[1], STDOUT_FILENO);
								dup2(err_pfd[1],STDERR_FILENO);
								
								
								execv(exec_path,exec_argv);
							}
							else
							{
								wait(NULL);

								bzero(buffer,15000);

								if(fd_in != 0)
									close(fd_in);
								
								if(number_pipe_type == 1)
								{
									close(pfd[1]);

									if(read(pfd[0],buffer,sizeof(buffer)) < 0)
										printf("Error read\n");

									if(write(err_pfd[1],buffer,strlen(buffer)) < 0) 
										error("ERROR writing to socket");

									close(err_pfd[1]);
									close(pfd[0]);

									fd_in = err_pfd[0];
								}
							   	else
							   	{
							   		close(err_pfd[1]);
							   		close(pfd[1]);

							   		int len = 0;
							   		if(len = read(err_pfd[0],buffer,sizeof(buffer)) < 0)
										printf("Error read\n");

									if(strlen(buffer) > 0)
									{
							   			if (write(newsockfd,buffer,strlen(buffer)) < 0) 
											error("ERROR writing to socket");				
									}
							   		close(err_pfd[0]);

									fd_in = pfd[0];
							   	}


							   	if(do_pipe_cnt == pipe_number-1)
							   	{
							   		bzero(buffer,15000);
							   		read(fd_in,buffer,sizeof(buffer));
							   		close(fd_in);
							   	}
								if(number_pipe_out_cnt > 0)
								{
									close(np_out[0]);
									number_pipe_out_cnt = 0;
								}
								
								
							}
						}
						else
						{
							if(number_pipe == 0)
							{
								end_by_unknown = 1;
								bzero(buffer,15000);
								strcpy(buffer,"Unknown command:[");
								strcat(buffer,exec_argv[0]);
								strcat(buffer,"].\n");
								n = write(newsockfd,buffer,strlen(buffer));

								if (n < 0) error("ERROR writing to socket");
								break;
							}
							else
							{

								number_pipe_fd[number_pipe_cnt] = fd_in;
								number_pipe_line[number_pipe_cnt] = atoi(exec_argv[0]);
								++number_pipe_cnt;
								number_pipe_type_array[number_pipe_cnt] = number_pipe_type;

							}
							
						}	
					}
					


					if(end_by_unknown != 1 && number_pipe == 0)
					{

						if(write_file == 1)
						{
							FILE *f = fopen(file_name, "w");
							if (f == NULL)
							{
								printf("Error opening file!\n");
								exit(1);
							}
							
							fprintf(f, buffer);
							
							fclose(f);	
						}
						else if(fifo_write == 1)
						{

							FILE* f;
							char fifo_write_path[100];

							sprintf(fifo_write_path,"/net/gcs/105/0556506/tmp/w%dto%d",current_id+1,fifo_dest_id);

							f = fopen(fifo_write_path,"w");
							
							fprintf(f,buffer);

							fclose(f);
						}
						else
						{				
							n = write(newsockfd,buffer,strlen(buffer));
						 		
							if (n < 0) error("ERROR writing to socket");
						}	
					}
				}
				

				exit(0);
				break;
			default :			
				close(newsockfd);
				break;
			case -1:
				printf("SOCKET FORK ERROR\n");
				break;
		}
		
	}

	if(shmdt(shm_ptr) <0) perror("shm detach error");

	sem_close(clisem);
	close(sockfd);
	return 0; 

}